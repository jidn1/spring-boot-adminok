package com.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @author jidn
 * @Date 2020/9/25
 */
@Configuration
@Data
@ConfigurationProperties("")
@Component
public class CrawlerUrl {

    private String cnBlog;

    private String zhihu;

    private String hupu;

    private String baidu;

    private String itHome;




}
