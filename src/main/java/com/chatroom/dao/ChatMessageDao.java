package com.chatroom.dao;

import com.mvc.base.dao.BaseDao;
import com.chatroom.model.ChatMessage;


/**
 * <p> ChatMessageDao </p>
 * @author: jidn
 * @Date :  2020-04-03 18:34:52 
 */
public interface ChatMessageDao extends  BaseDao<ChatMessage, Integer> {

}
